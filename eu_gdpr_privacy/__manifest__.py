# -*- coding: utf-8 -*-
# Copyright 2020-Today TechKhedut.
# Part of TechKhedut. See LICENSE file for full copyright and licensing details.
{
    "name" : "EU GDPR / GDPR Privacy / Website GDPR",
    "version" : "15.0.0.0",
    "category" : "website",
    'summary': 'This app makes to Odoo compatible with EU GDPR and DPA.',
    "description": """
        - Allow user to access their data
        - Allow user to remove thier data
        - Easy to configure in backend
        - Neat & Clean UI/UX
    """,
    'category': 'Website/GDPR',
    'author': 'TechKhedut Inc.',
    'company': 'TechKhedut Inc.',
    'maintainer': 'TechKhedut Inc.',
    'website': "https://www.techkhedut.com",
    "depends" : [
        'base',
        'web',
        'portal',
        'website'
    ],
    "data": [
        'security/ir.model.access.csv',
        'security/ir_rules.xml',
        'report/user_request_report.xml',
        'data/data.xml',
        'views/eu_gdpr_templates.xml',
        'views/gdpr_configuration_views.xml',
        'views/gdpr_request_views.xml',
        'views/gdpr_template_views.xml',
    ],
    'assets': {
        'web.assets_frontend': [
            'eu_gdpr_privacy/static/src/css/style.scss',
            'eu_gdpr_privacy/static/src/js/eu_gdpr.js'
        ],
    },
    'images': ['static/description/eu_gdpr.gif'],
    'qweb': [],
    'license': 'OPL-1',
    "auto_install": False,
    "installable": True,
    'application': True,
    'price': 45,
    'currency': 'EUR',
}
