odoo.define('eu_gdpr_privacy.gdpr_data', function (require) {
'use strict';
    require('web.dom_ready');
    var core = require('web.core');
    var ajax = require('web.ajax');

    $( ".btn.download" ).each(function(index) {
        $(this).on("click", function(){
            var id = $(this).attr('value'); 
            ajax.jsonRpc("/my/download", 'call', {
                'res_id': id,
            }).then(function (data) {
                $("#downloadreq").modal('show');
            });
        });
    });

    $( ".btn.deleted" ).each(function(index) {
        $(this).on("click", function(){
            var id = $(this).attr('value'); 
            ajax.jsonRpc("/my/deleted", 'call', {
                'res_id': id,
            }).then(function (data) {
                $("#deletereq").modal('show');
            });
        });
    });

    $( ".btn.done" ).each(function(index) {
        $(this).on("click", function(){
            window.location.reload()
        });
    });
});