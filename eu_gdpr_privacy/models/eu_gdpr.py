# -*- coding: utf-8 -*-
# Copyright 2020 - Today TechKhedut.
# Part of TechKhedut. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError



class GDPRTemplate(models.Model):
    _name = 'gdpr.template'
    _description = 'GDPR Types'

    name = fields.Char('Name')
    description = fields.Text('Description')
    url = fields.Char('Detailed Page URL')
    selection_type = fields.Selection(
        [('user', 'User'), ('address', 'Address')], string="Type of Data", default="user")
    active = fields.Boolean(string='Active', default=True)
    image = fields.Binary(string='Image')


class GDPRRequest(models.Model):
    _name = 'gdpr.request'
    _description = 'GDPR User Request'
    _order = 'create_date desc'

    name = fields.Char('Name')
    partner_id = fields.Many2one('res.partner', string='Requested By')
    gdpr_id = fields.Many2one('gdpr.template', string='GDPR Type')
    selection_type = fields.Selection([('s_obj', 'Specific Details'), (
        'all_obj', 'All Details')], string="Action For", default="s_obj")
    request_type = fields.Selection(
        [('download', 'Download'), ('delete', 'Delete')], string="Request Type", default="download")
    state = fields.Selection([('pending', 'In Progress'), ('done', 'Done'),
                              ('cancel', 'Cancel')], string="Status", default="pending")
    create_date = fields.Datetime('Created On')
    is_wipe = fields.Boolean('Is data removed ?', readonly=True, default=False)

    def mark_done(self):
        for request in self:
            if request.request_type == 'delete':
                request.wipe_data()
            request.write({'state': 'done'})
        return True

    def mark_cancel(self):
        for request in self:
            request.write({'state': 'cancel' })

        return True

    def wipe_data(self):
        for request in self:
            if request.partner_id:
                request.partner_id.write({
                    'phone': str('N/a'),
                    'email': str('N/a'),
                    'street': str('N/a'),
                    'street2': str('N/a'),
                    'zip': str('N/a'),
                    'city': str('N/a'),
                    'state_id': False,
                    'country_id': False,
                    'mobile': str('N/a'),
                })
                request.write({'is_wipe': True})
            else:
                UserError(_('Partner Is missing !!!'))

        return True


class GDPRConfiguration(models.Model):
    _name = 'gdpr.config'
    _description = 'GDPR Configuration'
    _rec_name = 'title'

    title = fields.Char('GDPR Title')
    description = fields.Html('Description')
    remove_desc = fields.Html('Removal Warning Message')

    @api.model
    def default_get(self, fields):
        res = super(GDPRConfiguration, self).default_get(fields)
        config_id = self.env['gdpr.config'].sudo().search(
            [], order="id desc", limit=1)
        if config_id:
            res.update({
                'title': config_id.title,
                'description': config_id.description,
                'remove_desc': config_id.remove_desc,
            })
        return res

    def save(self):
        for config in self:
            vals = {
                'title': config.title,
                'description': config.description,
                'remove_desc': config.remove_desc,
            }
            config.sudo().write(vals)
        return {'type': 'ir.actions.act_window_close'}