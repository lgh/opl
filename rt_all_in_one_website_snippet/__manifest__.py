# -*- coding: utf-8 -*-

{
    "name": "All in one Website Snippet | All in one snippets",
    "version":"15.0.1",
    "license": "OPL-1",
    "support": "relief.4technologies@gmail.com",  
    "author" : "Relief Technologies",    
    "category": "Website",
    "live_test_url": "",      
    "summary": "All in one Snippets, snippet, website snippet, banner &amp; slider, icon, image text, heading, img column, etc",
    "description": """
    """,
    "depends": [        
        'website',
    ],
    "data": [
        # "views/assets.xml",
        "views/snippet.xml",
        "views/snippet_panel.xml",
    ],
    'assets': {
        'web.assets_frontend': [
            '/rt_all_in_one_website_snippet/static/src/scss/style.scss',
        ],
    },
    "images": ["static/description/background.png",],              
    "installable": True,
    "application": True,
    "auto_install": False,
    "price": 25,
    "currency": "EUR"   
}
