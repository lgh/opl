# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# License URL : https://store.webkul.com/license.html/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
import odoo
from odoo import api, http, tools, _
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
# from odoo.addons.payment_paypal_express.controllers.main import PaypalExpressRedirect
from odoo import SUPERUSER_ID
from odoo.addons.payment.controllers.post_processing import PaymentPostProcessing

import logging
_logger = logging.getLogger(__name__)

# class WebsiteSale(WebsiteSale):

#     @http.route(['/shop/payment/transaction/',
#         '/shop/payment/transaction/<int:so_id>',
#         '/shop/payment/transaction/<int:so_id>/<string:access_token>'], type='json', auth="public", website=True)
#     def payment_transaction(self, acquirer_id, save_token=False, so_id=None, access_token=None, token=None, **kwargs):
#         """ Json method that creates a payment.transaction, used to create a
#         transaction when the user clicks on 'pay now' button. After having
#         created the transaction, the event continues and the user is redirected
#         to the acquirer website.

#         :param int acquirer_id: id of a payment.acquirer record. If not set the
#                                 user is redirected to the checkout page
#         """

#         public_paypal_checkout = kwargs.get('public_paypal_checkout',False)
#         # Ensure a payment acquirer is selected
#         if not acquirer_id:
#             return False

#         try:
#             acquirer_id = int(acquirer_id)

#         except:
#             return False

#         # Retrieve the sale order
#         if so_id:
#             env = request.env['sale.order']
#             domain = [('id', '=', so_id)]
#             if access_token:
#                 env = env.sudo()
#                 domain.append(('access_token', '=', access_token))
#             order = env.search(domain, limit=1)
#         else:
#             order = request.website.sale_get_order()

#         # Ensure there is something to proceed
#         if not order or (order and not order.order_line):
#             return False
#         if not public_paypal_checkout:
#             assert order.partner_id.id != request.website.partner_id.id

#         # Create transaction
#         vals = {'acquirer_id': acquirer_id,
#                 'return_url': '/shop/payment/validate'}

#         if save_token:
#             vals['type'] = 'form_save'
#         if token:
#             vals['payment_token_id'] = int(token)

#         transaction = order._create_payment_transaction(vals)

#         # store the new transaction into the transaction list and if there's an old one, we remove it
#         # until the day the ecommerce supports multiple orders at the same time
#         last_tx_id = request.session.get('__website_sale_last_tx_id')
#         last_tx = request.env['payment.transaction'].browse(last_tx_id).sudo().exists()
#         if last_tx:
#             PaymentPostProcessing.remove_transactions(last_tx)
#         PaymentPostProcessing.monitor_transactions(transaction)
#         request.session['__website_sale_last_tx_id'] = transaction.id
#         return transaction.render_sale_button(order)

class PaypalExpressCheckout(http.Controller):

    @http.route(['/get/paypal/acquirer/details',], type='json', auth="public", methods=['POST'], website=True)
    def get_paypal_acquirer_details(self, **post):
        acquirer = request.env['payment.acquirer'].sudo().search([('provider', '=', 'paypal_express'),('state','!=','disabled')], limit=1)
        if post.get("sale_order",False):
            order_id = request.env["sale.order"].sudo().browse(post.get("sale_order"))
        else:
            order_id = request.website.sale_get_order()
        data = {
            'acquirer_id':acquirer.id if acquirer else False,
            'enable_pro':acquirer.product_paypal if acquirer else False,
            'enable_cart':acquirer.cart_paypal if acquirer else False,
            'currency_id' : request.website.currency_id.id,
            'partner_id'   : order_id.partner_id.id,
            'flow'          : 'redirect',
            'tokenization_requested' : False,
            'landing_route': "/shop/payment/validate",
            'access_token': order_id.access_token,
            'sale_order' : order_id.id,
            'amount'    :  order_id.amount_total
        }
        return data

    @http.route(['/get/product/order/details',], type='json', auth="public", methods=['POST'], website=True)
    def get_product_order_details(self, **post):
        _logger.info("-----------------------get_product_order_details--------------------------")
        product_id = post.get('product_id')
        set_qty = post.get('add_qty')
        sale_order = request.website.sudo().sale_get_order(force_create=True)
        if sale_order.state != 'draft':
            request.session['sale_order_id'] = None
            sale_order = request.website.sudo().sale_get_order(force_create=True)
        sale_order.order_line.sudo().unlink()
        sale_order._cart_update(
            product_id=int(product_id),
            set_qty=set_qty,
        )
        request.session['sale_last_order_id'] = sale_order.id
        if not sale_order.access_token:
            sale_order._portal_ensure_token()
        return {
            'sale_order': sale_order.id
        }
