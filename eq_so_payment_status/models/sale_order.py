from odoo import api, models, fields, _


class SaleOrder(models.Model):
    _inherit = "sale.order"

    payment_transaction_count = fields.Integer(
        string="Number of payment transactions",
        compute='_compute_payment_transaction_count_new')
    payment_method_name = fields.Char(string="Payment Acquirer", compute="_compute_payment_transaction_count_new")
    payment_status = fields.Char(string="Payment Status", compute="_compute_payment_transaction_count_new", translate=True)

    def _compute_payment_transaction_count_new(self):
        for order in self:
            transaction_data = self.env['payment.transaction'].search([('sale_order_ids', 'in', order.ids)], order="id desc", limit=1)
            order.payment_transaction_count = len(transaction_data)
            payment_method_name = False
            payment_status = False
            if transaction_data:
                payment_method_name = transaction_data.acquirer_id.name
                state = dict(transaction_data.fields_get(['state'])['state']['selection'])[transaction_data.state]
                payment_status = state

            order.payment_method_name = payment_method_name
            order.payment_status = payment_status

    def action_view_transaction(self):
        action = {
            'type': 'ir.actions.act_window',
            'name': 'Payment Transactions',
            'res_model': 'payment.transaction',
        }
        if self.payment_transaction_count == 1:
            action.update({
                'res_id': self.env['payment.transaction'].search([('sale_order_ids', 'in', self.ids)], order="id desc", limit=1).id,
                'view_mode': 'form',
            })
        else:
            action.update({
                'view_mode': 'tree,form',
                'domain': [('sale_order_ids', 'in', self.ids)],
            })
        return action
