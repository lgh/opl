# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright 2019 EquickERP
#
##############################################################################
{
    'name': "Sale order Payment Status",
    'category': 'Sales',
    'version': '15.0.1.0',
    'author': 'Equick ERP',
    'description': """
        This Module allows you to see the payment details of website sales order.
        * Allows user to see the payment details of Website sale order.
    """,
    'summary': """ This Module allows you to see the payment details of website sales order. sale order payment status | so payment status | payment status in sale order | sale payment status | sale payment status  | website payment status. """,
    'depends': ['base', 'website_sale', 'sale_management'],
    'price': 10,
    'currency': 'EUR',
    'license': 'OPL-1',
    'website': "",
    'data': [
        'views/sale_view.xml'
    ],
    'demo': [],
    'images': ['static/description/main_screenshot.png'],
    'installable': True,
    'auto_install': False,
    'application': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: